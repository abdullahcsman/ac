package com.example.abdullah.architecturecomponent.repository;

import android.app.Application;
import android.arch.lifecycle.LiveData;
import android.os.AsyncTask;

import com.example.abdullah.architecturecomponent.database.ItuneRoomDatabase;
import com.example.abdullah.architecturecomponent.database.ItuneRoomModel;
import com.example.abdullah.architecturecomponent.database.ItuneRoomModelDao;

import java.util.List;

public class ItunesRepository {

    private ItuneRoomModelDao ituneDao;
private LiveData<List<ItuneRoomModel>> getItunes;

    public ItunesRepository(Application application){
        ItuneRoomDatabase db= ItuneRoomDatabase.getInstance(application);
        ituneDao= db.ituneDao();
        getItunes= ituneDao.getItunes();

 }

    public LiveData<List<ItuneRoomModel>> getItunes()
    {
        return getItunes;
    }
public void insertItunes(ItuneRoomModel ituneRoomModel){

        new insertAsyncTask(ituneDao).execute(ituneRoomModel);
}

private class insertAsyncTask extends AsyncTask<ItuneRoomModel,Void,Void>
{
     ItuneRoomModelDao idao;

    insertAsyncTask(ItuneRoomModelDao ir)
    {idao=ir;}

    @Override
    protected Void doInBackground(ItuneRoomModel... ituneRoomModels) {
        idao.insertItune(ituneRoomModels[0]);
        return null;
    }
}





//    public LiveData<List<ItuneRoomModel>> getListOfItunes(){
//
//        return ituneDao.getItunes();
//
//    }
//
//    public void insertNewListItem(ItuneRoomModel listItem){
//
//        ituneDao.insertItune(listItem);
//
//    }
//
//    public void deleteListItem(ItuneRoomModel listItem){
//
//        ituneDao.deleteAll(listItem);
//
//    }
}
