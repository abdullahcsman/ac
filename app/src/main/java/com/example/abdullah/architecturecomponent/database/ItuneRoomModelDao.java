package com.example.abdullah.architecturecomponent.database;

import android.arch.lifecycle.LiveData;
import android.arch.persistence.room.Dao;
import android.arch.persistence.room.Delete;
import android.arch.persistence.room.Insert;
import android.arch.persistence.room.OnConflictStrategy;

import java.util.List;

import retrofit2.http.Query;

@Dao
public interface ItuneRoomModelDao {
    @android.arch.persistence.room.Query("Select * from Itunes_table")
    LiveData<List<ItuneRoomModel>> getItunes();

@Insert(onConflict = OnConflictStrategy.REPLACE)
    void insertItune(ItuneRoomModel ituneRoomModel);

@Delete
    void deleteAll(ItuneRoomModel ituneRoomModel);

}
