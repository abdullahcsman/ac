package com.example.abdullah.architecturecomponent.model;

import android.app.Application;
import android.arch.lifecycle.AndroidViewModel;
import android.arch.lifecycle.LiveData;
import android.arch.lifecycle.MutableLiveData;
import android.arch.lifecycle.ViewModel;

import com.example.abdullah.architecturecomponent.database.ItuneRoomModel;
import com.example.abdullah.architecturecomponent.repository.ItunesRepository;

import java.util.List;
// TO connect with Repository
public class ItunesViewModel extends AndroidViewModel {

    private ItunesRepository itunesRepository;
// The object to be passed from repository, to view model to UI
    private LiveData<List<ItuneRoomModel>> listLiveData;

    public ItunesViewModel(Application application)
    {
        super(application);
        itunesRepository= new ItunesRepository(application);
        listLiveData=itunesRepository.getItunes();

    }
    public LiveData<List<ItuneRoomModel>> getItunes()
    {
        return listLiveData;
    }

public void insert(ItuneRoomModel ituneRoomModel)
{
    itunesRepository.insertItunes(ituneRoomModel);
}


//MutableLiveData<ItuneRoomModel> mutableLiveData;
//    public ItunesViewModel()
//    {
//       mutableLiveData=new MutableLiveData<>();
//    }
//
//    public MutableLiveData<ItuneRoomModel> getMutableLiveData() {
//        return mutableLiveData;
//    }
//
//    public void setMutableLiveData(ItuneRoomModel ituneRoomModel) {
//
//        mutableLiveData.postValue(ituneRoomModel);
//    }
}
