package com.example.abdullah.architecturecomponent.database;

import android.arch.persistence.room.Database;
import android.arch.persistence.room.Room;
import android.arch.persistence.room.RoomDatabase;
import android.content.Context;
import android.database.sqlite.SQLiteDatabase;

@Database(entities = {ItuneRoomModel.class}, version = 1)
public abstract class ItuneRoomDatabase extends RoomDatabase{

    public abstract ItuneRoomModelDao ituneDao();

    private static final String DATABASE_NAME = "Itunes_table";

    // For Singleton instantiation
    private static final Object LOCK = new Object();
    private static volatile ItuneRoomDatabase sInstance;

    public static ItuneRoomDatabase getInstance(Context context) {
        if (sInstance == null) {
            synchronized (LOCK) {
                if (sInstance == null) {
                    sInstance = Room.databaseBuilder(context.getApplicationContext(),
                            ItuneRoomDatabase.class, ItuneRoomDatabase.DATABASE_NAME).fallbackToDestructiveMigration().build();
                }
            }
        }
        return sInstance;
    }
public  static  void destroyInstance()
{
    sInstance = null;
}
//public abstract ItuneRoomModelDao getItemRoom();
}
