package com.example.abdullah.architecturecomponent.adapter;

import android.app.Activity;
import android.content.SharedPreferences;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.example.abdullah.architecturecomponent.R;
import com.example.abdullah.architecturecomponent.data.ItuneData;
import com.example.abdullah.architecturecomponent.data.ItuneModel;
import com.example.abdullah.architecturecomponent.network.ApiClient;
import com.squareup.picasso.Picasso;

import java.util.List;

public class ItunesAdapter extends BaseAdapter {

    List<ItuneData> im;
    Activity ma;


public ItunesAdapter(List<ItuneData> im, Activity ma)
{
    this.im=im;
    this.ma=ma;
}
    @NonNull
    @Override
    public int getCount() {
        return im.size();
    }

    @Override
    public Object getItem(int i) {
        return im.get(i);
    }

    @Override
    public long getItemId(int i) {
        return im.indexOf(getItem(i));
    }

    @Override
    public View getView(int i, View view, ViewGroup viewGroup) {
        ViewHolder holder = null;

        LayoutInflater mInflater = (LayoutInflater)ma.getSystemService(Activity.LAYOUT_INFLATER_SERVICE);
        if (view == null) {
            view = mInflater.inflate(R.layout.listfile, null);
            holder = new ViewHolder();
            holder.textView_artist = (TextView) view.findViewById(R.id.textView_Artist);
            holder.textView_song = (TextView) view.findViewById(R.id.textView_Song);
            holder.imageView = (ImageView) view.findViewById(R.id.imageView);
            view.setTag(holder);
        }
        else {
            holder = (ViewHolder) view.getTag();
        }

        ItuneData rowItem = im.get(i);

        holder.textView_artist.setText(rowItem.getArtistName());
        holder.textView_song.setText(rowItem.getCollectionCensoredName());
        if(rowItem.getCollectionViewUrl() != null) {
            if (!rowItem.getCollectionViewUrl().equalsIgnoreCase("")) {

                Picasso.with(ma).load(rowItem.getArtworkUrl60()).into(holder.imageView);
            }
        }


        return view;
    }


private class ViewHolder {
    ImageView imageView;
    TextView textView_artist;
    TextView textView_song;
}}