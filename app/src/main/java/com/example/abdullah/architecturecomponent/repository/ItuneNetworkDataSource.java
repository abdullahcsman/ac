package com.example.abdullah.architecturecomponent.repository;

import android.arch.lifecycle.LiveData;
import android.arch.lifecycle.MutableLiveData;

import com.example.abdullah.architecturecomponent.database.ItuneRoomModel;

public class ItuneNetworkDataSource {

    private final MutableLiveData<ItuneRoomModel[]> mItune;

    public ItuneNetworkDataSource()
    {
        mItune = new MutableLiveData<ItuneRoomModel[]>();
    }

    public LiveData<ItuneRoomModel[]> getCurrentWeatherForecasts() {
        return mItune;
    }
}
