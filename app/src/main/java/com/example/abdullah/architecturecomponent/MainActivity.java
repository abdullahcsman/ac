package com.example.abdullah.architecturecomponent;

import android.app.Activity;
import android.arch.lifecycle.ViewModelProviders;
import android.os.Handler;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.telecom.Call;
import android.widget.ListView;

import com.example.abdullah.architecturecomponent.adapter.ItunesAdapter;
import com.example.abdullah.architecturecomponent.data.ItuneData;
import com.example.abdullah.architecturecomponent.data.ItuneModel;
import com.example.abdullah.architecturecomponent.database.ItuneRoomModel;
import com.example.abdullah.architecturecomponent.model.ItunesViewModel;
import com.example.abdullah.architecturecomponent.network.ApiClient;
import com.example.abdullah.architecturecomponent.network.ApiInterface;

import java.util.List;

import retrofit2.Callback;
import retrofit2.Response;

public class MainActivity extends AppCompatActivity {
    SwipeRefreshLayout mSwipeRefreshLayout;
    private ItunesAdapter mHomeAdapter;
    private ListView mListView;
    List<ItuneData> quiz_details;
    private Activity mActivity;
    ItunesViewModel itunesViewModel;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        mActivity = this;
        mListView = (ListView)findViewById(R.id.listview);
        mSwipeRefreshLayout = (SwipeRefreshLayout) findViewById(R.id.swipeToRefresh);

        // Getting View Model Class
       // itunesViewModel = ViewModelProviders.of(this).get(ItunesViewModel.class);


        ApiInterface apiInterface= ApiClient.getClient().create(ApiInterface.class);
        retrofit2.Call<ItuneModel> ituneModelCall=apiInterface.getResult();
    ituneModelCall.enqueue(new Callback<ItuneModel>() {
        @Override
        public void onResponse(retrofit2.Call<ItuneModel> call, Response<ItuneModel> response) {

            if (response.isSuccessful()) {
                if (response != null){
                    quiz_details = response.body().getResults();
                    mHomeAdapter = new ItunesAdapter(quiz_details, mActivity);
                    mListView.setAdapter(mHomeAdapter);
                for (int i = 0; i < quiz_details.size(); i++) {
                    String a = quiz_details.get(i).getArtistName();
                    String b = quiz_details.get(i).getCollectionCensoredName();
                    String c = quiz_details.get(i).getArtworkUrl60();

                    ItuneRoomModel ituneRoom = new ItuneRoomModel(a, b, c);
                    itunesViewModel.insert(ituneRoom);

                }

//                Handler handler = new Handler();
//                handler.postDelayed(new Runnable() {
//                @Override
//                public void run() {
//
//                            }
//
//                    }, 3000);
//
            }
        }}
        @Override
        public void onFailure(retrofit2.Call<ItuneModel> call, Throwable t) {

        }
    });
    }
}
