package com.example.abdullah.architecturecomponent.database;

import android.arch.persistence.room.ColumnInfo;
import android.arch.persistence.room.Entity;
import android.arch.persistence.room.Ignore;
import android.arch.persistence.room.PrimaryKey;
import android.support.annotation.NonNull;

@Entity (tableName = "Itunes_table")
public class ItuneRoomModel {

    @PrimaryKey(autoGenerate = true)
    @ColumnInfo(name = "id")
    private int id;

    @NonNull
    @ColumnInfo(name = "artistName")
    private String artist_name;

    @NonNull
    @ColumnInfo(name = "collectionName")
    private String song_name;

    @NonNull
    @ColumnInfo(name = "artworkUrl60")
    private String image_URL;

//@Ignore
//    public ItuneRoomModel(int id,@NonNull String artist_name, @NonNull String song_name, @NonNull String image_URL)
//    {
//        this.id=id;
//        this.artist_name=artist_name;
//        this.song_name=song_name;
//        this.image_URL=image_URL;
//    }

    public ItuneRoomModel(@NonNull String artist_name, @NonNull String song_name, @NonNull String image_URL)
    {
        this.artist_name=artist_name;
        this.song_name=song_name;
        this.image_URL=image_URL;
    }

    @NonNull
    public String getArtist_name() {
        return artist_name;
    }

    @NonNull
    public String getSong_name() {
        return song_name;
    }

    @NonNull
    public String getImage_URL() {
        return image_URL;
    }

    public void setArtist_name(@NonNull String artist_name) {
        this.artist_name = artist_name;
    }

    public void setSong_name(@NonNull String song_name) {
        this.song_name = song_name;
    }

    public void setImage_URL(@NonNull String image_URL) {
        this.image_URL = image_URL;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }
}
