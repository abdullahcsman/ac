package com.example.abdullah.architecturecomponent.network;

import com.example.abdullah.architecturecomponent.data.ItuneModel;

import retrofit2.Call;
import retrofit2.http.GET;

public interface ApiInterface {
    @GET("lookup?amgArtistId=468749,5723&entity=album&limit=5")
    Call<ItuneModel> getResult();

}
